import React, { useState, FormEvent } from 'react'
import { useHistory } from 'react-router-dom'

import PageHeader from '../../components/PageHeader';
import Input from '../../components/Input';
import Textarea from '../../components/Textarea';
import Select from '../../components/Select';

import './styles.css'

import warningIcon from '../../assets/images/icons/warning.svg'
import api from '../../services/api';

function TeacherForm() {

    const history = useHistory();

    const [name, setName] = useState('');
    const [avatar, setAvatar] = useState('');
    const [whatsapp, setWhatsapp] = useState('');
    const [bio, setBio] = useState('');

    const [subject, setSubject] = useState('');
    const [cost, setCost] = useState('');


    const [scheduleItems, setScheduleItems] = useState([
        { week_day: 0, from:'', to: ''},
    ]); 
    
    function addNewScheduleItem(){
        setScheduleItems([
            ...scheduleItems,
            {
                week_day: 0,
                from: '',
                to: ''
            }
        ]);

    }

    function handleCreateClass(eve: FormEvent){

        eve.preventDefault();

        api.post('/classes', {
            name,
            avatar,
            whatsapp,
            bio,
            subject,
            cost: Number(cost),
            schedule: scheduleItems,
        }).then(() => {
            alert('Cadastro realizado com sucesso');
            history.push('/')
        }).catch((err) => {
            alert('Erro ao cadastrar');
        })

        console.log({
            name, 
            avatar,
            whatsapp,
            bio,
            subject,
            cost,
            scheduleItems,
        });
    }

    function setScheduleItemValue(position: number, field: string, value: string){

        const updatedScheduleItems = scheduleItems.map((scheduleItem, index) => {
            if(index === position){
                return { ...scheduleItem, [field]: value }
            }

            return scheduleItem;
        });
        
        setScheduleItems(updatedScheduleItems);

    }

    return(
        <div id="page-teacher-form" className="container">
            <PageHeader 
                title="Que incrível que você quer dar aulas."
                description="O primeiro passo é preencher esse formulário de inscrição"
            />

            <main>
                <form onSubmit={handleCreateClass} >
                    <fieldset>
                        <legend>Seus dados</legend>

                        <Input 
                            name="name" 
                            label="Nome Completo" 
                            value={name} 
                            onChange={(eve) => { setName(eve.target.value) }}
                        />

                        <Input 
                            name="avatar" 
                            label="Avatar"
                            value={avatar} 
                            onChange={(eve) => { setAvatar(eve.target.value) }}
                        />

                        <Input 
                            name="whatsapp" 
                            label="Whatsapp"
                            value={whatsapp} 
                            onChange={(eve) => { setWhatsapp(eve.target.value) }}
                        />

                        <Textarea 
                            name="bio" 
                            label="Biografia"
                            value={bio} 
                            onChange={(eve) => { setBio(eve.target.value) }}
                        />

                    </fieldset>

                    <fieldset>
                        <legend>Sobre a aula</legend>

                        <Select 
                            name="subject" 
                            label="Matéria"
                            value={subject}
                            onChange={(eve) => { setSubject(eve.target.value) }}
                            options={[
                                {value: 'Artes', label: 'Artes'},
                                {value: 'Biologia', label: 'Biologia'},
                                {value: 'Física', label: 'Física'},
                                {value: 'Química', label: 'Química'},
                                {value: 'Educação Física', label: 'Educação Física'},
                                {value: 'Matemática', label: 'Matemática'},
                                {value: 'Portugês', label: 'Portugês'},
                                {value: 'História', label: 'História'},
                                {value: 'Geografia', label: 'Geografia'},
                            ]}
                        />

                        <Input 
                            name="cost" 
                            label="Custo da sua aula por hora"
                            value={cost}
                            onChange={(eve) => { setCost(eve.target.value) }}
                        />
                        
                    </fieldset>

                    <fieldset>
                        <legend>
                            <div> Horários disponíveis </div>
                            <button type="button" onClick={addNewScheduleItem} > 
                                + Novo horário 
                            </button> 
                        </legend>

                        {scheduleItems.map((scheduleItem, index) => {
                            return (
                            <div className="schedule-item" key={scheduleItem.week_day} >
                                <Select 
                                    name="week_day" 
                                    label="Dia da semana"
                                    value={scheduleItem.week_day}
                                    onChange={eve => setScheduleItemValue(index, 'week_day', eve.target.value)}
                                    options={[
                                        {value: '0', label: 'Domingo'},
                                        {value: '1', label: 'Segunda-feira'},
                                        {value: '2', label: 'Terça-feira'},
                                        {value: '3', label: 'Quarta-feira'},
                                        {value: '4', label: 'Quinta-feira'},
                                        {value: '5', label: 'Sexta-feira'},
                                        {value: '6', label: 'Sábado'},
                                    ]}
                                />

                                <Input 
                                    name="from"
                                    label="Das" 
                                    type="time" 
                                    value={scheduleItem.from}
                                    onChange={eve => setScheduleItemValue(index, 'from', eve.target.value)}
                                />
                                <Input 
                                    name="to"
                                    label="Até" 
                                    type="time" 
                                    value={scheduleItem.to}
                                    onChange={eve => setScheduleItemValue(index, 'to', eve.target.value)}
                                />
                            </div>
                        );
                        })}
                    </fieldset>

                    <footer>
                        <p>
                            <img src={warningIcon} alt="Aviso Importante"/>
                            Importante! <br/>
                            Prencha todos os dados
                        </p>

                        <button type="submit" >
                            Salvar Cadastro
                        </button>
                    </footer>
                </form>
            </main>
        </div>
    );
}

export default TeacherForm;